Docker environment for Symfony4
===============================

Get started
-----------

_This project contain **Makefile**_

1. Build container `make dev`
2. Run the command line of the container `docker-compose exec php7 bash`
3. Install dependencies `composer install`

Another commands
----------------

* Start container `make start`
* Stop container `make stop`

Run tests
---------

1. Run the command line of the container `docker-compose exec php7 bash`
2. Run behat scenarios `bin/behat`

