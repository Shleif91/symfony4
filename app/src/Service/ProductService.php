<?php

declare(strict_types=1);

namespace App\Service;

use App\DataManager\ProductDataManager;
use App\Entity\Product;
use Doctrine\ORM\Query;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductService
{
    /** @var ProductDataManager */
    private $dataManager;

    public function __construct(ProductDataManager $dataManager)
    {
        $this->dataManager = $dataManager;
    }

    public function getProductById(string $id): Product
    {
        $product = $this->dataManager->findProductById($id);

        if (is_null($product)) {
            throw new NotFoundHttpException();
        }

        return $product;
    }

    public function getAllProducts(): ?array
    {
        return $this->dataManager->findAllProducts();
    }

    public function getAllProductsQuery(): Query
    {
        return $this->dataManager->getAllProductsQuery();
    }

    public function saveProduct(Product $product): void
    {
        $this->dataManager->save($product);
    }

    public function removeProduct(Product $product): void
    {
        $this->dataManager->remove($product);
    }
}