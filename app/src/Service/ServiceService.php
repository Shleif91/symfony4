<?php

declare(strict_types=1);

namespace App\Service;

use App\DataManager\ServiceDataManager;
use App\Entity\Service;
use Doctrine\ORM\Query;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ServiceService
{
    /** @var ServiceDataManager */
    private $dataManager;

    public function __construct(ServiceDataManager $dataManager)
    {
        $this->dataManager = $dataManager;
    }

    public function getServiceById(string $id): Service
    {
        $service = $this->dataManager->findServiceById($id);

        if (is_null($service)) {
            throw new NotFoundHttpException();
        }

        return $service;
    }

    public function getAllServices(): ?array
    {
        return $this->dataManager->findAllServices();
    }

    public function getAllServicesQuery(): Query
    {
        return $this->dataManager->getAllServicesQuery();
    }

    public function saveService(Service $service): void
    {
        $this->dataManager->save($service);
    }

    public function removeService(Service $service): void
    {
        $this->dataManager->remove($service);
    }
}