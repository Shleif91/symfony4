<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Service\ProductService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\RouteResource("Product", pluralize=false)
 */
class ProductController extends FOSRestController implements ClassResourceInterface
{
    /** @var ProductService */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function postAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $form = $this->createForm(ProductType::class, new Product());
        $form->submit($data);

        if (false === $form->isValid()) {
            return $form;
        }

        $this->productService->saveProduct($form->getData());

        return $this->view(
            ['status' => 'ok'],
            Response::HTTP_CREATED
        );
    }

    public function getAction(string $id)
    {
        return $this->productService->getProductById($id);
    }

    public function cgetAction(Request $request)
    {
        $pagination = $this->get('knp_paginator')->paginate(
            $this->productService->getAllProductsQuery(),
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 5)
        );

        return [
            'data' => $pagination->getItems(),
            'meta' => $pagination->getPaginationData()
        ];
    }

    public function putAction(Request $request, string $id)
    {
        $data = json_decode($request->getContent(), true);
        $product = $this->productService->getProductById($id);

        $form = $this->createForm(ProductType::class, $product);
        $form->submit($data);

        if (false === $form->isValid()) {
            return $form;
        }

        $this->productService->saveProduct($product);

        return $this->view(
            ['status' => 'ok'],
            Response::HTTP_NO_CONTENT
        );
    }

    public function patchAction(Request $request, string $id)
    {
        $data = json_decode($request->getContent(), true);
        $product = $this->productService->getProductById($id);

        $form = $this->createForm(ProductType::class, $product);
        $form->submit($data, false);

        if (false === $form->isValid()) {
            return $form;
        }

        $this->productService->saveProduct($product);

        return $this->view(
            ['status' => 'ok'],
            Response::HTTP_NO_CONTENT
        );
    }

    public function deleteAction(string $id)
    {
        $product = $this->productService->getProductById($id);
        $this->productService->removeProduct($product);

        return $this->view(
            ['status' => 'ok'],
            Response::HTTP_NO_CONTENT
        );
    }
}
