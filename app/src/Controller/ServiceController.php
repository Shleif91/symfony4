<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Service;
use App\Form\ServiceType;
use App\Service\ServiceService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\RouteResource("Service", pluralize=false)
 */
class ServiceController extends FOSRestController implements ClassResourceInterface
{
    /** @var ServiceService */
    private $serviceService;

    public function __construct(ServiceService $serviceService)
    {
        $this->serviceService = $serviceService;
    }

    public function postAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $form = $this->createForm(ServiceType::class, new Service());
        $form->submit($data);

        if (false === $form->isValid()) {
            return $form;
        }

        $this->serviceService->saveService($form->getData());

        return $this->view(
            ['status' => 'ok'],
            Response::HTTP_CREATED
        );
    }

    public function getAction(string $id)
    {
        return $this->serviceService->getServiceById($id);
    }

    public function cgetAction(Request $request)
    {
        $pagination = $this->get('knp_paginator')->paginate(
            $this->serviceService->getAllServicesQuery(),
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 5)
        );

        return [
            'data' => $pagination->getItems(),
            'meta' => $pagination->getPaginationData()
        ];
    }

    public function putAction(Request $request, string $id)
    {
        $data = json_decode($request->getContent(), true);
        $service = $this->serviceService->getServiceById($id);

        $form = $this->createForm(ServiceType::class, $service);
        $form->submit($data);

        if (false === $form->isValid()) {
            return $form;
        }

        $this->serviceService->saveService($service);

        return $this->view(
            ['status' => 'ok'],
            Response::HTTP_NO_CONTENT
        );
    }

    public function patchAction(Request $request, string $id)
    {
        $data = json_decode($request->getContent(), true);
        $service = $this->serviceService->getServiceById($id);

        $form = $this->createForm(ServiceType::class, $service);
        $form->submit($data, false);

        if (false === $form->isValid()) {
            return $form;
        }

        $this->serviceService->saveService($service);

        return $this->view(
            ['status' => 'ok'],
            Response::HTTP_NO_CONTENT
        );
    }

    public function deleteAction(string $id)
    {
        $service = $this->serviceService->getServiceById($id);
        $this->serviceService->removeService($service);

        return $this->view(
            ['status' => 'ok'],
            Response::HTTP_NO_CONTENT
        );
    }
}
