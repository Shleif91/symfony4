<?php

declare(strict_types=1);

namespace App\DataManager;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;

class ProductDataManager
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var ProductRepository */
    private $productRepository;

    public function __construct(EntityManagerInterface $em, ProductRepository $productRepository)
    {
        $this->em = $em;
        $this->productRepository = $productRepository;
    }

    public function findProductById(string $id): ?Product
    {
        return $this->productRepository->find($id);
    }

    public function findAllProducts(): ?array
    {
        return $this->productRepository->findAll();
    }

    public function getAllProductsQuery(): Query
    {
        return $this->productRepository->getAllProductsQuery();
    }

    public function save(Product $product): void
    {
        $this->em->persist($product);
        $this->em->flush();
    }

    public function remove(Product $product): void
    {
        $this->em->remove($product);
        $this->em->flush();
    }
}