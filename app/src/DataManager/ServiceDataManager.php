<?php

declare(strict_types=1);

namespace App\DataManager;

use App\Entity\Service;
use App\Repository\ServiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;

class ServiceDataManager
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var ServiceRepository */
    private $serviceRepository;

    public function __construct(EntityManagerInterface $em, ServiceRepository $serviceRepository)
    {
        $this->em = $em;
        $this->serviceRepository = $serviceRepository;
    }

    public function findServiceById(string $id): ?Service
    {
        return $this->serviceRepository->find($id);
    }

    public function findAllServices(): ?array
    {
        return $this->serviceRepository->findAll();
    }

    public function getAllServicesQuery(): Query
    {
        return $this->serviceRepository->getAllServicesQuery();
    }

    public function save(Service $service): void
    {
        $this->em->persist($service);
        $this->em->flush();
    }

    public function remove(Service $service): void
    {
        $this->em->remove($service);
        $this->em->flush();
    }
}