<?php

declare(strict_types=1);

namespace App\Serializer\Normalizer;

use App\Entity\Service;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ServiceNormalizer implements NormalizerInterface
{
    /** {@inheritdoc} */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'type' => [
                'id' => $object->getType()->getId(),
                'title' => $object->getType()->getTitle()
            ],
            'created_at' => $object->getCreatedAt()->format(DATE_ATOM),
            'updated_at' => $object->getUpdatedAt()->format(DATE_ATOM),
        ];
    }

    /** {@inheritdoc} */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Service;
    }
}