<?php

namespace App\Repository;

use App\Entity\TypeOfService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypeOfService|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeOfService|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeOfService[]    findAll()
 * @method TypeOfService[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeOfServiceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypeOfService::class);
    }
}
