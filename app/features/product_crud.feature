@product
Feature: To ensure the API is responding in a simple manner

  In order to offer a working product
  As a conscientious software developer
  I need to ensure my JSON API is functioning

  Scenario: Can add a new Product
    Given the request body is:
      """
      {
        "title": "Awesome new Product",
        "cost": 7
      }
      """
    When I request "/product" using HTTP POST
    Then the response code is 201

  Scenario: Can get a single Product
    Given I have Product with the following details:
      | id                                   | title        | cost |
      | 00000000-0000-0000-0000-000000000000 | Test product | 10   |
    When I request "/product/00000000-0000-0000-0000-000000000000" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "id": "00000000-0000-0000-0000-000000000000",
        "title": "Test product",
        "cost": 10,
        "created_at": "2018-04-09T13:00:00+00:00",
        "updated_at": "2018-04-09T13:00:00+00:00"
      }
      """

  Scenario: Can get a collection of Products
    Given I have Product with the following details:
      | id                                   | title        | cost |
      | 00000000-0000-0000-0000-000000000000 | Test product | 10   |
    When I request "/product" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "data": [
            {
                "id": "00000000-0000-0000-0000-000000000000",
                "title": "Test product",
                "cost": 10,
                "created_at": "2018-04-09T13:00:00+00:00",
                "updated_at": "2018-04-09T13:00:00+00:00"
            }
        ],
        "meta": {
            "last": 1,
            "current": 1,
            "numItemsPerPage": 5,
            "first": 1,
            "pageCount": 1,
            "totalCount": 1,
            "pageRange": 1,
            "startPage": 1,
            "endPage": 1,
            "pagesInRange": [
                1
            ],
            "firstPageInRange": 1,
            "lastPageInRange": 1,
            "currentItemCount": 1,
            "firstItemNumber": 1,
            "lastItemNumber": 1
        }
      }
      """

  Scenario: Can update an existing Product - PUT
    Given I have Product with the following details:
      | id                                   | title        | cost |
      | 00000000-0000-0000-0000-000000000000 | Test product | 10   |
    And the request body is:
      """
      {
        "title": "Renamed an product",
        "cost": 9
      }
      """
    When I request "/product/00000000-0000-0000-0000-000000000000" using HTTP PUT
    Then the response code is 204

  Scenario: Can update an existing Product - PATCH
    Given I have Product with the following details:
      | id                                   | title        | cost |
      | 00000000-0000-0000-0000-000000000000 | Test product | 10   |
    And the request body is:
      """
      {
        "cost": 8
      }
      """
    When I request "/product/00000000-0000-0000-0000-000000000000" using HTTP PATCH
    Then the response code is 204

  Scenario: Can delete an Product
    Given I have Products with the following details:
      | id                                   | title        | cost |
      | 00000000-0000-0000-0000-000000000000 | Test product | 10   |
    When I request "/product/00000000-0000-0000-0000-000000000000" using HTTP GET
    Then the response code is 200
    When I request "/product/00000000-0000-0000-0000-000000000000" using HTTP DELETE
    Then the response code is 204
    When I request "/product/00000000-0000-0000-0000-000000000000" using HTTP GET
    Then the response code is 404

  Scenario: Can get a collection of Products with 1 per page
    Given I have Products with the following details:
      | id                                   | title        | cost |
      | 00000000-0000-0000-0000-000000000000 | Test product | 10   |
    And there are Products with the following details:
      | title           | cost |
      | Test Product 1  | 1    |
      | Test Product 2  | 1    |
      | Test Product 3  | 1    |
      | Test Product 4  | 1    |
      | Test Product 5  | 1    |
      | Test Product 6  | 1    |
      | Test Product 7  | 1    |
      | Test Product 8  | 1    |
      | Test Product 9  | 1    |
      | Test Product 10 | 1    |
    When I request "/product?limit=1" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "data": [
          {
            "id": "00000000-0000-0000-0000-000000000000",
            "title": "Test product",
            "cost": 10,
            "created_at": "2018-04-09T13:00:00+00:00",
            "updated_at": "2018-04-09T13:00:00+00:00"
          }
        ],
        "meta": {
          "last": 11,
          "current": 1,
          "numItemsPerPage": 1,
          "first": 1,
          "pageCount": 11,
          "totalCount": 11,
          "pageRange": 5,
          "startPage": 1,
          "endPage": 5,
          "next": 2,
          "pagesInRange": [
              1,
              2,
              3,
              4,
              5
          ],
          "firstPageInRange": 1,
          "lastPageInRange": 5,
          "currentItemCount": 1,
          "firstItemNumber": 1,
          "lastItemNumber": 1
        }
      }
      """