@service
Feature: To ensure the API is responding in a simple manner

  In order to offer a working service
  As a conscientious software developer
  I need to ensure my JSON API is functioning

  Background:
    Given I have Type of Service with the following details:
      | id | title     |
      | 1  | Test type |

  Scenario: Can add a new Service
    Given the request body is:
      """
      {
        "title": "Awesome new Service",
        "type": 1
      }
      """
    When I request "/service" using HTTP POST
    Then the response code is 201

  Scenario: Can get a single Service
    Given I have Service with the following details:
      | id                                   | title        | type_id |
      | 00000000-0000-0000-0000-000000000000 | Test service | 1       |
    When I request "/service/00000000-0000-0000-0000-000000000000" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "id": "00000000-0000-0000-0000-000000000000",
        "title": "Test service",
        "type": {
          "id": 1,
          "title": "Test type"
        },
        "created_at": "2018-04-24T13:00:00+00:00",
        "updated_at": "2018-04-24T13:00:00+00:00"
      }
      """

  Scenario: Can get a collection of Services
    Given I have Service with the following details:
      | id                                   | title        | type_id |
      | 00000000-0000-0000-0000-000000000000 | Test service | 1       |
    When I request "/service" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
        {
          "data": [
            {
              "id": "00000000-0000-0000-0000-000000000000",
              "title": "Test service",
              "type": {
                  "id": 1,
                  "title": "Test type"
              },
              "created_at": "2018-04-24T13:00:00+00:00",
              "updated_at": "2018-04-24T13:00:00+00:00"
            }
          ],
          "meta": {
            "last": 1,
            "current": 1,
            "numItemsPerPage": 5,
            "first": 1,
            "pageCount": 1,
            "totalCount": 1,
            "pageRange": 1,
            "startPage": 1,
            "endPage": 1,
            "pagesInRange": [
                1
            ],
            "firstPageInRange": 1,
            "lastPageInRange": 1,
            "currentItemCount": 1,
            "firstItemNumber": 1,
            "lastItemNumber": 1
          }
        }
      """

  Scenario: Can update an existing Service - PUT
    Given I have Service with the following details:
      | id                                   | title        | type_id |
      | 00000000-0000-0000-0000-000000000000 | Test service | 1       |
    When the request body is:
      """
      {
        "title": "Renamed Service",
        "type": 1
      }
      """
    When I request "/service/00000000-0000-0000-0000-000000000000" using HTTP PUT
    Then the response code is 204

  Scenario: Can update an existing Service - PATCH
    Given I have Service with the following details:
      | id                                   | title        | type_id |
      | 00000000-0000-0000-0000-000000000000 | Test service | 1       |
    When the request body is:
      """
      {
        "title": "Renamed Service"
      }
      """
    When I request "/service/00000000-0000-0000-0000-000000000000" using HTTP PATCH
    Then the response code is 204

  Scenario: Can delete an Service
    Given I have Service with the following details:
      | id                                   | title        | type_id |
      | 00000000-0000-0000-0000-000000000000 | Test service | 1       |
    When I request "/service/00000000-0000-0000-0000-000000000000" using HTTP GET
    Then the response code is 200
    When I request "/service/00000000-0000-0000-0000-000000000000" using HTTP DELETE
    Then the response code is 204
    When I request "/service/00000000-0000-0000-0000-000000000000" using HTTP GET
    Then the response code is 404

  Scenario: Can get a collection of Services with 1 per page
    Given I have Service with the following details:
      | id                                   | title        | type_id |
      | 00000000-0000-0000-0000-000000000000 | Test service | 1       |
    And there are Services with the following details:
      | title           | type_id |
      | Test Service 1  | 1       |
      | Test Service 2  | 1       |
      | Test Service 3  | 1       |
      | Test Service 4  | 1       |
      | Test Service 5  | 1       |
      | Test Service 6  | 1       |
      | Test Service 7  | 1       |
      | Test Service 8  | 1       |
      | Test Service 9  | 1       |
      | Test Service 10 | 1       |
    When I request "/service?limit=1" using HTTP GET
    Then the response code is 200
    And the response body contains JSON:
      """
      {
        "data": [
          {
            "id": "00000000-0000-0000-0000-000000000000",
            "title": "Test service",
            "type": {
                "id": 1,
                "title": "Test type"
            },
            "created_at": "2018-04-24T13:00:00+00:00",
            "updated_at": "2018-04-24T13:00:00+00:00"
          }
        ],
        "meta": {
          "last": 11,
          "current": 1,
          "numItemsPerPage": 1,
          "first": 1,
          "pageCount": 11,
          "totalCount": 11,
          "pageRange": 5,
          "startPage": 1,
          "endPage": 5,
          "next": 2,
          "pagesInRange": [
              1,
              2,
              3,
              4,
              5
          ],
          "firstPageInRange": 1,
          "lastPageInRange": 5,
          "currentItemCount": 1,
          "firstItemNumber": 1,
          "lastItemNumber": 1
        }
      }
      """