@edge_case @service
Feature: Provide insight into how Service behaves on the unhappy path

  In order to eliminate bad Service data
  As a JSON API developer
  I need to ensure Service data meets expected criteria

  Background:
    Given I have Type of Service with the following details:
      | id | title     |
      | 1  | Test type |

  Scenario: Must have a non-blank title
    Given the request body is:
      """
      {
        "title": "",
        "type": 1
      }
      """
    When I request "/service" using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
      """
      {
          "code": 400,
          "message": "Validation Failed",
          "errors": {
              "children": {
                  "title": {
                      "errors": [
                          "This value should not be blank."
                      ]
                  },
                  "type": []
              }
          }
      }
      """

  Scenario: Must have a non-blank cost
    Given the request body is:
      """
      {
        "title": "Test",
        "type": ""
      }
      """
    When I request "/service" using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
      """
      {
          "code": 400,
          "message": "Validation Failed",
          "errors": {
              "children": {
                  "title": [],
                  "type": {
                      "errors": [
                          "This value should not be blank."
                      ]
                  }
              }
          }
      }
      """

  Scenario: Must have a cost grater that zero
    Given the request body is:
      """
      {
        "title": "Test title",
        "type": -3
      }
      """
    When I request "/service" using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
      """
      {
          "code": 400,
          "message": "Validation Failed",
          "errors": {
              "children": {
                  "title": [],
                  "type": {
                      "errors": [
                          "This value is not valid."
                      ]
                  }
              }
          }
      }
      """

  Scenario: Service ID must exist for GET method
    Given I request "/service/111111-1111-1111-1111-111111111111" using HTTP GET
    Then the response code is 404
