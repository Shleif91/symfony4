@edge_case @product
Feature: Provide insight into how Product behaves on the unhappy path

  In order to eliminate bad Product data
  As a JSON API developer
  I need to ensure Product data meets expected criteria

  Scenario: Must have a non-blank title
    Given the request body is:
      """
      {
        "title": "",
        "cost": "7"
      }
      """
    When I request "/product" using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
      """
      {
          "code": 400,
          "message": "Validation Failed",
          "errors": {
              "children": {
                  "title": {
                      "errors": [
                          "This value should not be blank."
                      ]
                  },
                  "cost": []
              }
          }
      }
      """

  Scenario: Must have a non-blank cost
    Given the request body is:
      """
      {
        "title": "Test",
        "cost": ""
      }
      """
    When I request "/product" using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
      """
      {
          "code": 400,
          "message": "Validation Failed",
          "errors": {
              "children": {
                  "title": [],
                  "cost": {
                      "errors": [
                          "This value should not be blank."
                      ]
                  }
              }
          }
      }
      """

  Scenario: Must have a cost grater that zero
    Given the request body is:
      """
      {
        "title": "Test title",
        "cost": -3
      }
      """
    When I request "/product" using HTTP POST
    Then the response code is 400
    And the response body contains JSON:
      """
      {
          "code": 400,
          "message": "Validation Failed",
          "errors": {
              "children": {
                  "title": [],
                  "cost": {
                      "errors": [
                          "This value should be greater than 0."
                      ]
                  }
              }
          }
      }
      """

  Scenario: Product ID must exist for GET method
    Given I request "/product/111111-1111-1111-1111-111111111111" using HTTP GET
    Then the response code is 404
