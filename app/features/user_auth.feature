@user_auth
Feature: To ensure the API is responding in a simple manner

  In order to offer a working service
  As a conscientious software developer
  I need to ensure my JSON API is functioning

  Scenario: User can register
    Given the request body is:
      """
      {
        "username": "User",
        "password": 123456,
        "email": "testuser@gmail.com"
      }
      """
    When I request "/register/" using HTTP POST
    Then the response code is 201

  Scenario: User can login
    Given the request body is:
      """
      {
        "username": "User",
        "password": 123456,
        "email": "testuser@gmail.com"
      }
      """
    When I request "/register/" using HTTP POST
    Then the response code is 201