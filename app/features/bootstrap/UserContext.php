<?php

use Behat\Behat\Context\Context;
use Doctrine\ORM\EntityManagerInterface;

class UserContext implements Context
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
}
