<?php

use App\Entity\Service;
use App\Entity\TypeOfService;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Doctrine\ORM\EntityManagerInterface;

class ServiceCrudContext implements Context
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var \App\Repository\TypeOfServiceRepository */
    private $typeOfServiceRepository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->typeOfServiceRepository = $this->em->getRepository(TypeOfService::class);
    }

    /** @Given there are Services with the following details: */
    public function thereAreServicesWithTheFollowingDetails(TableNode $services)
    {
        foreach ($services->getColumnsHash() as $params) {
            $type = $this->typeOfServiceRepository->find($params['type_id']);
            $service = new Service();
            $service->setTitle($params['title']);
            $service->setType($type);

            $this->em->persist($service);
        }

        $this->em->flush();
    }

    /** @Given I have Service(s) with the following details: */
    public function iHaveServiceWithTheFollowingDetails(TableNode $services)
    {
        foreach ($services->getColumnsHash() as $params) {
            $this->createTestService(
                $params['id'],
                $params['title'],
                $params['type_id']
            );
        }
    }

    /** @Given I have Type(s) of Service(s) with the following details: */
    public function iHaveTypeOfServiceWithTheFollowingDetails(TableNode $services)
    {
        foreach ($services->getColumnsHash() as $params) {
            $this->createTestTypeOfService(
                $params['id'],
                $params['title']
            );
        }
    }

    private function createTestTypeOfService(int $id, string $title)
    {
        $query = sprintf(
            'INSERT INTO types_of_services (id, title) VALUES ("%s", "%s")',
            $id,
            $title
        );

        $this->em->getConnection()->exec($query);
    }

    private function createTestService(string $id, string $title, int $type)
    {
        $query = sprintf(
            'INSERT INTO services (id, title, type_id, created_at, updated_at) VALUES ("%s", "%s", %d, "2018-04-24 13:00:00", "2018-04-24 13:00:00")',
            $id,
            $title,
            $type
        );

        $this->em->getConnection()->exec($query);
    }
}
