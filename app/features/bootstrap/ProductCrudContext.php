<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Doctrine\ORM\EntityManagerInterface;

class ProductCrudContext implements Context
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /** @Given there are Products with the following details: */
    public function thereAreProductsWithTheFollowingDetails(TableNode $products)
    {
        foreach ($products->getColumnsHash() as $params) {
            $product = new \App\Entity\Product();
            $product->setTitle($params['title']);
            $product->setCost($params['cost']);

            $this->em->persist($product);
        }

        $this->em->flush();
    }

    /** @Given I have Product(s) with the following details: */
    public function iHaveProductWithTheFollowingDetails(TableNode $products)
    {
        foreach ($products->getColumnsHash() as $params) {
            $this->createTestProduct(
                $params['id'],
                $params['title'],
                $params['cost']
            );
        }
    }

    private function createTestProduct(string $id, string $title, float $cost)
    {
        $query = sprintf(
            'INSERT INTO products (id, title, cost, created_at, updated_at) VALUES ("%s", "%s", %d, "2018-04-09 13:00:00", "2018-04-09 13:00:00")',
            $id,
            $title,
            $cost
        );

        $this->em->getConnection()->exec($query);
    }
}
