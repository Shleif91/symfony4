<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Doctrine\ORM\EntityManagerInterface;
use Imbo\BehatApiExtension\Context\ApiContext;

class FeatureContext implements Context
{
    use \Subcontexts\EraseDatabaseTrait;

    /** @var ApiContext */
    private $apiContext;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /** @BeforeScenario */
    public function gatherContexts(BeforeScenarioScope $scope)
    {
        $this->apiContext = $scope->getEnvironment()->getContext(
            ApiContext::class
        );
    }
}
