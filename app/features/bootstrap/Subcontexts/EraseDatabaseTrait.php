<?php

namespace Subcontexts;

trait EraseDatabaseTrait
{
    /** @BeforeScenario */
    public function dropAllData()
    {
        $this->em->createQuery('DELETE App:Product')->execute();
        $this->em->createQuery('DELETE App:TypeOfService')->execute();
        $this->em->createQuery('DELETE App:Service')->execute();
    }
}